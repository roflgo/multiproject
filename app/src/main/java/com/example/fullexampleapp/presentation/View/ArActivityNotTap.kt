package com.example.fullexampleapp.presentation.View

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fullexampleapp.R
import com.example.fullexampleapp.databinding.ActivityArNotTapBinding
import com.google.ar.core.Pose
import com.google.ar.core.TrackingState
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.FrameTime
import com.google.ar.sceneform.Scene
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

class ArActivityNotTap : AppCompatActivity(), Scene.OnUpdateListener{
    private lateinit var binding: ActivityArNotTapBinding
    private lateinit var arFragment: ArFragment
    private var lamp : TransformableNode? = null
    private lateinit var modelRenderable: ModelRenderable
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArNotTapBinding.inflate(layoutInflater)
        setContentView(binding.root)
        arFragment = supportFragmentManager.findFragmentById(R.id.ar_second) as ArFragment
        getPermission()
    }

    private fun getPermission() {
        Dexter.withContext(this)
            .withPermissions(Manifest.permission.CAMERA)
            .withListener(object: MultiplePermissionsListener{
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    arFragment.arSceneView.scene.addOnUpdateListener(this@ArActivityNotTap)
                }
                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {

                }
            }).check()
    }

    private fun setupAR(vector3: Vector3) {
        changeModel()

        val session = arFragment.arSceneView.session
        val frame = arFragment.arSceneView.arFrame ?: return
        if (frame.camera.trackingState != TrackingState.TRACKING) {
            return
        }
        val pose = Pose.makeTranslation(vector3.x, vector3.y, vector3.z);

        val anchorNode = AnchorNode(session?.createAnchor(pose))
        anchorNode.setParent(arFragment.arSceneView.scene)
        if(lamp != null){
//            arFragment.arSceneView.scene.removeChild(lamp)
//            anchorNode.anchor?.detach()
            return

        }

            lamp = TransformableNode(arFragment.transformationSystem)
            lamp!!.setParent(anchorNode)
            lamp!!.renderable = modelRenderable
            lamp!!.localScale = vector3
            lamp!!.scaleController.maxScale = 0.5f
            lamp!!.scaleController.minScale = 0.01f
            lamp!!.select()


    }

    private fun changeModel() {
        ModelRenderable.Builder()
            .setSource(this, R.raw.and)
            .build()
            .thenAccept() {
                modelRenderable = it
            }
    }

    override fun onUpdate(p0: FrameTime?) {
        setupAR(Vector3(0.1f,0.1f,0.1f))
    }



}