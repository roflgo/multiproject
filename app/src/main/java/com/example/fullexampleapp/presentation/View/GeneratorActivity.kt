package com.example.fullexampleapp.presentation.View

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.fullexampleapp.BuildConfig
import com.example.fullexampleapp.R
import com.example.fullexampleapp.common.Utils
import com.example.fullexampleapp.common.toSha256
import com.example.fullexampleapp.databinding.ActivityGeneratorBinding
import com.example.fullexampleapp.presentation.ViewModel.RouteViewModel
import com.google.maps.android.PolyUtil
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class GeneratorActivity : AppCompatActivity(R.layout.activity_generator) {

    private val vm: RouteViewModel by viewModel()
    private val binding: ActivityGeneratorBinding by viewBinding()

    @SuppressLint("HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope
        val uid = UUID.randomUUID()
        val modelPhone = (Build.MODEL + " " + Build.BRAND + " ("
                + Build.VERSION.RELEASE + ")"
                + " API-" + Build.VERSION.SDK_INT)
        Log.d("BuildConfig", "${uid}, ${BuildConfig.APPLICATION_ID}, $modelPhone")
        val strings = "hash string".toSha256()
        Log.d("HashingString", strings)

        initObserver()
        getRoute()
    }

    private fun getRoute() {
        val stringPath = "37.734791,55.877934;37.732965,55.885787?alternatives=false"
        vm.getRoute(stringPath)
    }

    private fun initObserver() {
        vm.successRouteMutableData.observe(this) {
            Log.d("route_poly", it.routes[0].geometry)
        }
        vm.errorMutableLiveData.observe(this) {
            Utils.defaultAlert(this, it.toString())
        }
    }
}