package com.example.fullexampleapp.presentation.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.fullexampleapp.domain.usecase.SaveDataUseCase

class UiTestViewModel(
    private val saveDataUseCase: SaveDataUseCase
) : ViewModel(){
    val resultSaveMutableLiveData = MutableLiveData<Boolean>()

    fun saveTokenData(token: String) {
        resultSaveMutableLiveData.value = saveDataUseCase.execute(token)
    }
}