package com.example.fullexampleapp.presentation.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.fullexampleapp.R
import com.example.fullexampleapp.adapters.RecyclerTestAdapter
import com.example.fullexampleapp.common.RecyclerTest
import com.example.fullexampleapp.databinding.ActivityUiTestBinding
import com.example.fullexampleapp.presentation.View.FragmentUiTest.UiTestFragment
import com.example.fullexampleapp.presentation.ViewModel.UiTestViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class UiTestActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUiTestBinding
    private val vm: UiTestViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUiTestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        vm.saveTokenData("token")

        val dataRecycler = listOf(
            RecyclerTest(
                "First Element"
            ),
            RecyclerTest(
                "Second Element"
            ),
            RecyclerTest(
                "Three Element"
            ),
            RecyclerTest(
                "Four Element"
            ),
            RecyclerTest(
                "First Element"
            ),
            RecyclerTest(
                "Second Element"
            ),
            RecyclerTest(
                "Three Element"
            ),
            RecyclerTest(
                "Four Element"
            ),
            RecyclerTest(
                "First Element"
            ),
            RecyclerTest(
                "Second Element"
            ),
            RecyclerTest(
                "Three Element"
            ),
            RecyclerTest(
                "Four Element"
            )
        ,RecyclerTest(
                "First Element"
            ),
            RecyclerTest(
                "Second Element"
            ),
            RecyclerTest(
                "Three Element"
            ),
            RecyclerTest(
                "Four Element"
            )
        )

        binding.recyclerTestLayout.adapter = RecyclerTestAdapter(this, dataRecycler)

        binding.buttonTestLayout.setOnClickListener {
            binding.fragmentTransition.visibility = View.VISIBLE
            supportFragmentManager.beginTransaction().replace(R.id.fragment_transition, UiTestFragment()).commit()
        }

        vm.resultSaveMutableLiveData.observe(this) {
            Toast.makeText(this, "$it", Toast.LENGTH_SHORT).show()
        }
    }
}