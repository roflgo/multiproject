package com.example.fullexampleapp.presentation.View

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.fullexampleapp.data.network.toGeoPoint
import com.example.fullexampleapp.databinding.ActivityOpenStreetBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.maps.android.PolyUtil
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.XYTileSource
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Polyline
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay

class OpenStreetActivity : AppCompatActivity() {

    private lateinit var mapView: MapView
    private lateinit var locatinOverlay: MyLocationNewOverlay
    private lateinit var binding: ActivityOpenStreetBinding
    private lateinit var fused: FusedLocationProviderClient
    private lateinit var mapController: IMapController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOpenStreetBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mapView = binding.mapOpenStreet

        val tileSource = XYTileSource(
            "MAP",
            10,20,
            256, ".png",
            arrayOf(
                "https://map.madskill.ru/osm/15/54.1842/45.1771/"
            )
        ) // Ставим долбанную карту /

        decodeAddress()
        mapView.setTileSource(tileSource)
        mapController = mapView.controller
        mapController.setZoom(15.0) // Зумим карту

        fused = LocationServices.getFusedLocationProviderClient(this) // Жопа для получения твоей локации
        Configuration.getInstance().load(applicationContext, getSharedPreferences("main", Context.MODE_PRIVATE)) // Устанавливаем карту

        getPermission() // Заправшиваем доступ к локации
    }

    private fun getLocation() { // Подключаем определение локации
        locatinOverlay = MyLocationNewOverlay(GpsMyLocationProvider(this), mapView)
        locatinOverlay.enableMyLocation()
        locatinOverlay.enableFollowLocation()
        mapView.overlays.add(locatinOverlay)
    }

    private fun decodeAddress() { // Функция получения адреса кординат
        val geocoder = Geocoder(this)
        val result = geocoder.getFromLocationName("Екатеринбург Линейная 46", 1) // Получение кординат по адресу
        Log.d("result_geocode", "${result[0].latitude}, ${result[0].longitude}")

        val resultAddress = geocoder.getFromLocation(result[0].latitude, result[0].longitude, 1) // Получение по кординатам адрес
        Log.d("result_geocode", "${resultAddress[0]}")
    }

    private fun generateRoute() {
        val poly = Polyline(mapView)
        val polyDecode = PolyUtil.decode("sadf") // Сюда hint вкладывается
        poly.setPoints(polyDecode.toGeoPoint()) // Получение поинтов
        poly.width = 5f // Ширина дороги но и без нее ворк
        mapView.overlays.add(poly) // Добавление маршрута
    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        fused.lastLocation.addOnSuccessListener {
            val currentGeo = GeoPoint(it.latitude, it.longitude)
            addMarkerOnMap(currentGeo)
            moveCameraOnLocation(currentGeo)
        }
    }

    private fun moveCameraOnLocation(geo: GeoPoint) {
        mapController.animateTo(geo)
    }

    private fun getPermission() {
        Dexter.withContext(this).withPermissions(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        ).withListener(object: MultiplePermissionsListener{
            override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                getLocation()
                getCurrentLocation()
            }

            override fun onPermissionRationaleShouldBeShown(
                p0: MutableList<PermissionRequest>?,
                p1: PermissionToken?
            ) {

            }
        }).check()
    }

    private fun addMarkerOnMap(locationMarker: GeoPoint) { // Функция для добавления маркера на карту
        val marker = Marker(mapView)
        marker.position = locationMarker // Устанавливаем позицию маркера
        marker.title = "Тут явно что-то есть" // Ставит title в маркер
//        marker.icon = resources.getDrawable(R.drawable.ic_launcher_background) // Отвечает за смену иконки внутри маркера
//        marker.image = resources.getDrawable(R.drawable.ic_launcher_background) // Отвечает за смену иконки самого маркера
        marker.setOnMarkerClickListener(object : Marker.OnMarkerClickListener {
            override fun onMarkerClick(marker: Marker?, mapView: MapView?): Boolean {
                Toast.makeText(this@OpenStreetActivity, "Это маркер сука", Toast.LENGTH_SHORT).show() // Позиция маркера тут находится
                marker!!.showInfoWindow() // Отображения экрана с данными например title
                return true
            }
        })
        mapView.overlays.add(marker)
    }
}