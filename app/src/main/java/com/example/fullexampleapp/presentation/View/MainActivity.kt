package com.example.fullexampleapp.presentation.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputBinding
import androidx.lifecycle.ViewModelProvider
import com.example.fullexampleapp.R
import com.example.fullexampleapp.adapters.RecyclerAdapterMain
import com.example.fullexampleapp.databinding.ActivityMainBinding
import com.example.fullexampleapp.presentation.ViewModel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val vm: MainViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initObserver()

        vm.getMovies()
    }

    fun initObserver() {
        vm.resultData.observe(this) {
            binding.recyclerFirst.adapter = RecyclerAdapterMain(this, it)
        }
    }

    fun openViewPager2(view: View) {
        val intent = Intent(this, ViewPagerFirstActivity::class.java)
        startActivity(intent)
    }
}