package com.example.fullexampleapp.presentation.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.fullexampleapp.data.networkMapRoute.Resource
import com.example.fullexampleapp.data.networkMapRoute.RouteRepository
import com.example.fullexampleapp.data.networkMapRoute.RoutesResponse
import kotlinx.coroutines.launch

class TestOptionViewModel(
    private val routeRepository: RouteRepository
): ViewModel() {
    val routeResponse = MutableLiveData<RoutesResponse>()
    val errorResponse = MutableLiveData<String>()

    fun getRouteInfo(input: String) {
        viewModelScope.launch {
            val response = routeRepository.routeDrawer(input)
            when(response) {
                is Resource.Error -> {errorResponse.value = response.message}
                is Resource.Success -> {routeResponse.value = response.data}
            }
        }
    }
}