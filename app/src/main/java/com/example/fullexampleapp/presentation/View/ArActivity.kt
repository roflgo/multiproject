package com.example.fullexampleapp.presentation.View

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fullexampleapp.R
import com.example.fullexampleapp.databinding.ActivityArBinding
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode
import com.google.ar.sceneform.ux.TransformationSystem
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener

class ArActivity : AppCompatActivity() {
    private lateinit var modelRenderable: ModelRenderable
    private lateinit var arFragment: ArFragment
    private lateinit var binding: ActivityArBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArBinding.inflate(layoutInflater)
        setContentView(binding.root)
        arFragment = supportFragmentManager.findFragmentById(R.id.ar_scene) as ArFragment
        getPermission()
    }

    private fun getPermission() {
        Dexter.withContext(this)
            .withPermission(Manifest.permission.CAMERA)
            .withListener(object: PermissionListener{
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    setupArFragment()
                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {

                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {

                }
            })
            .check()
    }

    private fun setupArFragment() {
        ModelRenderable.Builder()
            .setSource(this, R.raw.and)
            .build()
            .thenAccept {render: ModelRenderable ->
                modelRenderable = render
            }

        arFragment.setOnTapArPlaneListener { hitResult, plane, motionEvent ->
            val anchore = hitResult.createAnchor()
            val anchoreNode = AnchorNode(anchore)
            anchoreNode.setParent(arFragment.arSceneView.scene)

            val lamp = TransformableNode(arFragment.transformationSystem) // Возможность трансформировать
            lamp.setParent(anchoreNode) // Присваиваем родителя
            lamp.renderable = modelRenderable // Устанавливаем модель
            lamp.scaleController.maxScale = 0.5f // Максимальный размер
            lamp.scaleController.minScale = 0.1f // Минимальный размер

            val startSize = Vector3(0.1f,0.1f,0.1f) // Изначальный размер модели
            lamp.localScale = startSize // Присваеваем старотовый размер

            lamp.select() // Выбираем нужную модель
        }
    }
}