package com.example.fullexampleapp.domain.repository

import com.example.fullexampleapp.data.network.MovieResult
import com.example.fullexampleapp.data.network.ResultPolyline

interface NetworkDomain {
    suspend fun getMovie(): MovieResult
    suspend fun getPolyline(): ResultPolyline
}