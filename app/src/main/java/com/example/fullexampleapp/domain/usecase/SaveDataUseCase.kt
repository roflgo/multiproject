package com.example.fullexampleapp.domain.usecase

import com.example.fullexampleapp.domain.repository.SaveDataRepository

class SaveDataUseCase(private val storage: SaveDataRepository) {
    fun execute(token: String): Boolean {
        return storage.saveData(token)
    }
}