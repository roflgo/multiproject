package com.example.fullexampleapp.domain.repository

interface SaveDataRepository {
    fun saveData(token: String): Boolean
}