package com.example.fullexampleapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.fullexampleapp.R
import com.example.fullexampleapp.common.RecyclerTest

class RecyclerTestAdapter(val context: Context, val blockData: List<RecyclerTest>): RecyclerView.Adapter<RecyclerTestAdapter.MyVH>() {
    class MyVH(itemView: View): RecyclerView.ViewHolder(itemView) {
        val textEdit = itemView.findViewById<TextView>(R.id.text_recycler_exp)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        val root = LayoutInflater.from(context).inflate(R.layout.test_recycler_block, parent, false)
        return MyVH(root)
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {
        holder.textEdit.text = blockData[position].text
        holder.textEdit.setOnClickListener {
            Toast.makeText(context, "sdkfksdjfksd", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return blockData.size
    }
}