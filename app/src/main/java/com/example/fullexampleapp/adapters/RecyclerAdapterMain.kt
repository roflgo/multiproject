package com.example.fullexampleapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.fullexampleapp.R
import com.example.fullexampleapp.common.Utils
import com.example.fullexampleapp.data.network.movies

class RecyclerAdapterMain(val context: Context, val movie: List<movies>): RecyclerView.Adapter<RecyclerAdapterMain.MyVH>() {
    class MyVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageMain = itemView.findViewById<ImageView>(R.id.image_main_recycler)
        val textMain = itemView.findViewById<TextView>(R.id.text_main_recycler)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        val root = LayoutInflater.from(context).inflate(R.layout.recycler_adapter_main, parent, false)
        return MyVH(root)
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {
        Glide.with(context).load(Utils.baseImageUrl() + movie[position].poster).into(holder.imageMain)
        holder.textMain.text = movie[position].name
    }

    override fun getItemCount(): Int {
        return movie.size
    }
}