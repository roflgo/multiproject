package com.example.fullexampleapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.fullexampleapp.R
import com.example.fullexampleapp.data.network.movies


class ViewPagerAdapterFirst(val context: Context, val movies: List<movies>): RecyclerView.Adapter<ViewPagerAdapterFirst.MyVH>() {
    class MyVH(itemView: View): RecyclerView.ViewHolder(itemView) {
        val textMain: TextView = itemView.findViewById(R.id.viewPagerFirstText)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        val root = LayoutInflater.from(context).inflate(R.layout.view_pager_first_adapter, parent, false)
        return MyVH(root)
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {
        holder.textMain.text = movies[position].description
    }

    override fun getItemCount(): Int {
        return movies.size
    }
}