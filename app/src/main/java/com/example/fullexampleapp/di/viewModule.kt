package com.example.fullexampleapp.di

import com.example.fullexampleapp.presentation.ViewModel.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModule = module {
    viewModel {
        MainViewModel(get())
    }

    viewModel{
        ViewPagerFirstViewModel(get())
    }

    viewModel{
        RouteViewModel(get())
    }

    viewModel {
        TestOptionViewModel(get())
    }

    viewModel{
        UiTestViewModel(get())
    }
}