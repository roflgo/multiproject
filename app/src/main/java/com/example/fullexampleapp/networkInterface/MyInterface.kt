package com.example.fullexampleapp.networkInterface

import com.example.fullexampleapp.data.network.movies
import retrofit2.http.GET
import retrofit2.http.Query

interface MyInterface {
    @GET("movies")
    suspend fun getMovie(
        @Query("filter") filter: String,
    ): List<movies>
}