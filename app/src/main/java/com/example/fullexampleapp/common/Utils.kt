package com.example.fullexampleapp.common

import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.os.Message

object Utils {
    fun baseUrl() = "http://cinema.areas.su/"
    fun baseImageUrl() = "http://cinema.areas.su/up/images/"
    fun baseSearchUrl() = "https://overpass.madskill.ru/"
    fun baseRouteUrl() = "https://route.madskill.ru/"

    fun defaultAlert(context: Context, message: String) = AlertDialog.Builder(context)
        .setMessage(message)
        .create().show()

    fun checkConnection(context: Context): Boolean {
        val inter = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val infoConnection = inter.activeNetworkInfo
        return infoConnection!= null && infoConnection.isConnected
    }
}