package com.example.fullexampleapp.common

import com.google.android.gms.maps.model.LatLng
import org.osmdroid.util.GeoPoint
import java.security.MessageDigest

fun String.toSha256(): String {
    return hashString(this)
}

private fun hashString(input: String): String {
    return MessageDigest
        .getInstance("SHA-256")
        .digest(input.toByteArray())
        .fold("") {str, it ->
            str + "%02x".format(it)
        }
}

fun List<LatLng>.toGeoPoint(): List<GeoPoint> {
    val newList = this.map {
        GeoPoint(
            it.latitude,
            it.longitude
        )
    }
    return newList
}

fun LatLng.toGeoOne(): GeoPoint{
    return GeoPoint(this.latitude,
    this.longitude)
}