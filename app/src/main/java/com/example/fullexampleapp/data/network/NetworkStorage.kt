package com.example.fullexampleapp.data.network

import com.example.fullexampleapp.networkInterface.MyInterface
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class NetworkStorage(private val retrofit: MyInterface) {
    suspend fun getMovie(): MovieResult {
        return withContext(Dispatchers.IO) {
            try {
                val response = retrofit.getMovie("forMe")
                MovieResult.Success(response)
            } catch (e: HttpException) {
                val result = Gson().fromJson(e.response()?.errorBody()?.string(), ErrorServer::class.java)
                MovieResult.Error(result.details)
            } catch (e: Exception) {
                MovieResult.Error(e.localizedMessage!!)
            }
        }
    }
}