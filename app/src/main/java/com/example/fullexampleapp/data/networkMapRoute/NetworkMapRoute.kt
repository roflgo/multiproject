package com.example.fullexampleapp.data.networkMapRoute

import retrofit2.http.GET
import retrofit2.http.Path

interface NetworkMapRoute {
    @GET("route/v1/driving/{path}")
    suspend fun getPolylineRoute(@Path("path") path: String): RoutesResponse
}