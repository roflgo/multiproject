package com.example.fullexampleapp.data.storage

interface ISaveDataRepository {
    fun saveData(token: String): Boolean
}