package com.example.fullexampleapp.data.networkMapRoute

import android.util.Log
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class RouteRepository(private val retrofit: NetworkMapRoute) {
    suspend fun routeDrawer(input: String): Resource<RoutesResponse> {
        return withContext(Dispatchers.IO) {
            try {
               Resource.Success( retrofit.getPolylineRoute(input))
            } catch (e: HttpException) {
                val result = Gson().fromJson(e.response()?.errorBody()?.string(), ErrorResult::class.java)

                Log.d("wtf",e.localizedMessage+" "+ result.message)
                Resource.Error(result.message)
            } catch (e: Exception) {

                Log.d("wtf",e.localizedMessage)
                Resource.Error(e.localizedMessage)
            }
        }
    }
}